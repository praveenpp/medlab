

function SetMaxID() {

    // $('#frame').html('<html><head></head><body> <label for = "DocterID"> Docters Name :</label></body></html>');
    var ReqType = 'GetMaxID';
    $.ajax({
        type: "POST",
        url: "php/BillEntry.php",
        data: {Req: ReqType},
        success: function (data)
        {
            $('#BillNo').val(data);
            // $("#BillNo_1").val(data);
        }
    })
}
function SetSex() {

    var Temp = '<select><option value="1">Male</option><option value="2">Female</option></select>';
    $('#Sex').html(Temp);
}
function DiscountChange() {
    $("#Discount").change(function ()
    {
        var temp_amt = $("#TotalAmount").val() - $("#Discount").val();
        $("#AmountReceipt").val(temp_amt);
    })

}
function ReceiptAmount() {
    $("#AmountReceipt").change(function ()
    {
        var TempRcptAmt = $("#TotalAmount").val() - $("#AmountReceipt").val();
        $("#Discount").val(TempRcptAmt);
    })

}



function SetDocterID() {
    var BDCTableName = "docter_master";
    var DBCColumnID = "id";
    var DBCColumnName = 'name';

    $.ajax({
        type: "POST",
        url: "PHP/DBCombo.php",
        data: {TableName: BDCTableName, ColumnID: DBCColumnID, ColumnName: DBCColumnName},
        success: function (data) {
            $("#DocterID").html(data);
        }

    });
}
function SetTestGroup() {
    var BDCTableName = "test_group_master";
    var DBCColumnID = "group_id";
    var DBCColumnName = 'group_name';

    $.ajax({
        type: "POST",
        url: "PHP/DBCombo.php",
        data: {TableName: BDCTableName, ColumnID: DBCColumnID, ColumnName: DBCColumnName},
        success: function (data) {
            var rowCount = $('#TBillDt tr').length - 1;
            // alert(rowCount);
            $("#TestGroup_" + rowCount).html(data);
        }

    });
}

function InsertRow() {
    var rowCount = $('#TBillDt tr').length;
    var newRow = '<tr class ="TRBillDt"><td><input type="text" id="SrNo_' + rowCount + '"  value= "' + rowCount + '" class="SrNo" name="SrNo_' + rowCount + '" value="1"></td><td><select type="text" id="TestGroup_' + rowCount + '" class="TestGroup" name="TestGroup_' + rowCount + '"></select></td><td><select  type="text" id="Test_' + rowCount + '" class="Test" name="Test_' + rowCount + '"></select></td><td><input type="text" id="Amount_' + rowCount + '" class="Amount" name="Amount_' + rowCount + '"></td></tr>';
    $('#TBillDt').append(newRow);
    SetTestGroup();
    $("#SrNo_" + rowCount).attr("readonly", true)

}
function BillDtChange() {
    $("#TBillDt").change(function ()
    {
        var rowCount = $('#TBillDt tr').length;
        //$(this).css("background-color", "green");
        rowCount = rowCount - 1;
        //alert(rowCount);
        var lastcol = document.getElementById("TBillDt").rows[rowCount].cells[3].childNodes[0].value;
        if (lastcol) {
            InsertRow();

        }

    });
}
function SetTestID(GroupID, Position) {
    //var GroupID = $('#TestGroup_1').val();
    //alert(GroupID);
    var DBCTableName = 'test_master';
    var DBCColumnID = 'test_id';
    var DBCColumnName = 'test_name';
    var DBCWhereClause = 'group_id=' + GroupID;
    // alert(DBCWhereClause);
    $.ajax({
        type: "POST",
        url: "php/DYCombo.php",
        data: {TableName: DBCTableName, ColumnID: DBCColumnID, ColumnName: DBCColumnName, WhereClause: DBCWhereClause},
        success: function (data)
        {
            $('#Test_' + Position).html(data);
        }
    })
}
function TestGroupChange() {
    $(document.body).on('change', '.TestGroup', function () {
        var ID = $(this).attr('id');
        var Position = ID.split('_');
        Position = Position[1];
        // alert(POSITION);ar
        var GroupID = $('#TestGroup_' + Position).val();
        //alert(GroupID);
        SetTestID(GroupID, Position);
    })
}
function AmountChange() {
    $(document.body).on('keypress', '.Amount', function () {
        $("#Discount").val(0);
        var len = ($(".TRBillDt")).length;
        var temp_sum = 0;
        //  alert($("#TBillDt").length );
        for (var i = 1; i <= len; i++) {
            temp_sum = (temp_sum * 1) + ($("#Amount_" + i).val() * 1);
        }
        $("#TotalAmount").val(temp_sum);
        $("#AmountReceipt").val(temp_sum);
    })
}
function TestIDChange() {
    $(document.body).on('keypress', '.Test', function () {
        var ID = $(this).attr('id');
        var Position = ID.split('_');
        Position = Position[1];
        // alert(POSITION);ar
        var TestID = $('#Test_' + Position).val();
        //alert(GroupID);


        var TempPosition = Position;
        var TableLength = ($(".TRBillDt")).length;
        if (TempPosition == TableLength) {
            InsertRow();
        }
        if (TestID) {
            SetRate(TestID, Position);
        }

        setTimeout(function () {
            TotalSet();
        }, 1001);


    })

}
function TotalSet() {

    $("#Discount").val(0);
    var len = ($(".TRBillDt")).length;
    var temp_sum = 0;
    //  alert($("#TBillDt").length );
    for (var i = 1; i <= len; i++) {
        temp_sum = (temp_sum * 1) + ($("#Amount_" + i).val() * 1);
    }
    $("#TotalAmount").val(temp_sum);
    $("#AmountReceipt").val(temp_sum);
    //alert ($('#Amount_'+Position).val());
}

function SetRate(TestID, Position) {
    var Req = 'GetRate'
    $.ajax({
        type: "POST",
        url: "php/TestMaster.php",
        data: {TestID: TestID, Req: Req},
        success: function (data)
        {
            //alert(data);
            $('#Amount_' + Position).val(data);

            // alert(data);
        }
    })
    /*
     alert("Timer not working to create event");
     setTimeout(function() {
     TableClick();
     }, 100);
     */
    //////// Total Amount Setting
    /*
     
     setTimeout(function () {  alert("k");
     var len=($(".TRBillDt")).length;
     var temp_sum = 0;
     
     for(var i=1;i<len;i++){
     temp_sum =(temp_sum * 1)+ ($("#Amount_"+i).val()* 1);
     }
     $("#TotalAmount").val(temp_sum);},100);
     */

}
function Save() {
    $("#Save").click(function () {
        var TableLength = ($(".TRBillDt")).length;
        var TableCount = TableLength;
        if (Validate("PatientName") == 'N') {

            return;

        }
         if (Validate("DocterID") == 'N') {

             return;

           }
        if (NumberValidate("Age") == 'N') {

            return;

        }


        //  alert(TableLength);
        if (TableCount == 1) {
            TableCount = 2;
        }


        for (i = 1; i <= TableCount - 1; i++) {
            if (Validate('TestGroup_' + i) == 'N') {
                return;

            }
            if (Validate('Test_' + i) == 'N') {
                return;

            }
            if (Validate('Amount_' + i) == 'N') {
                return;

            }

        }
        
        var FormData = $("#FBillDt").serialize();
        // var TableLength = ($(".PropertyRow").length);

        var ReqType = {'Req': 'Save'};
        var Data = {'TableLength': TableLength};
        $.ajax({
            type: "POST",
            url: "php/BillEntry.php",
            data: FormData + '&' + $.param(ReqType) + '&' + $.param(Data),
            success: function (data) {
               
                var ConfirmStatus = confirm(data  + '  ' + 'Are You Want To Print');
                if (ConfirmStatus==true) {

                      BillPrint();
                    

                }
                
                $("#FBillDt")[0].reset(0); 
                var SlNo = 1;
                $('tr').remove();
                $('#TBillDt').append('<tr><th class="SrNoH">Sr No</th><th class="TestPropertyH">Test Group</th><th class="PropertyH">Test</th><th class="AmountH">Amount</th></tr>');
                $('#TBillDt').append('<tr class ="TRBillDt"><td><input type="text" id="SrNo_' + SlNo + '"  value= "' + SlNo + '" class="SrNo" name="SrNo_' + SlNo + '" value="1"></td><td><select type="text" id="TestGroup_' + SlNo + '" class="TestGroup" name="TestGroup_' + SlNo + '"></select></td><td><select  type="text" id="Test_' + SlNo + '" class="Test" name="Test_' + SlNo + '"></select></td><td><input type="text" id="Amount_' + SlNo + '" class="Amount" name="Amount_' + SlNo + '"></td></tr>');
                SetTestGroup();
                $("#SrNo_" + SlNo).attr("readonly", true);
                SetDate();
                SetMaxID();
            }

        })



    })
}

function BillNoChange() {
    $(document.body).on('change', '#BillNo', function () {
        var BillNo = $("#BillNo").val();
        var Req = "GetHDDetails";

        // bill hd setting
        $.ajax({
            type: "POST",
            url: "php/BillEntry.php",
            data: {'Req': Req, 'BillNo': BillNo},
            //data: {'TestID': TestID, 'Req': Req},
            success: function (data) {


                if (data == 'No Result') {

                    alert('Bill No Not Exist');
                    return;

                }


                var DataHD = data.split('~~');
                $('#TxnDate').val(DataHD[0]);
                $('#DocterID').val(DataHD[1]);
                $('#PatientName').val(DataHD[2]);
                $('#Age').val(DataHD[3]);
                var SexCode = DataHD[4] * 1;
                $('#Sex').val(SexCode);
                $('#Address1').val(DataHD[5]);
                $('#Address2').val(DataHD[6]);
                $('#Address3').val(DataHD[7]);
                $('#PhoneNo').val(DataHD[8]);
                $('#TotalAmount').val(DataHD[9]);
                $('#AmountReceipt').val(DataHD[10]);
                $('#Discount').val(DataHD[11]);
                $('#Hospital').val(DataHD[12]);
                $('#Sample').val(DataHD[13]);
            }
        })
        //bill dt detail setting
        var Req = "GetDTDetails";
        $.ajax({
            type: "POST",
            url: "php/BillEntry.php",
            data: {'Req': Req, 'BillNo': BillNo},
            success: function (data) {
                $('#DTTableDIV').html(data);
            }
        })

    }
    )

}
function NavigationSetdata(BillNo){
   
        // bill hd setting
        var Req = "GetHDDetails";
        $.ajax({
            type: "POST",
            url: "php/BillEntry.php",
            data: {'Req': Req, 'BillNo': BillNo},
            //data: {'TestID': TestID, 'Req': Req},
            success: function (data) {
                if (data == 'No Result') {

                    alert('Bill No Not Exist');
                    return;
                }
                var DataHD = data.split('~~');
                $('#TxnDate').val(DataHD[0]);
                $('#BillNo').val(BillNo);
                $('#DocterID').val(DataHD[1]);
                $('#PatientName').val(DataHD[2]);
                $('#Age').val(DataHD[3]);
                var SexCode = DataHD[4] * 1;
                $('#Sex').val(SexCode);
                $('#Address1').val(DataHD[5]);
                $('#Address2').val(DataHD[6]);
                $('#Address3').val(DataHD[7]);
                $('#PhoneNo').val(DataHD[8]);
                $('#TotalAmount').val(DataHD[9]);
                $('#AmountReceipt').val(DataHD[10]);
                $('#Discount').val(DataHD[11]);
                $('#Hospital').val(DataHD[12]);
                $('#Sample').val(DataHD[13]);
            }
        })
        //bill dt detail setting
        var Req = "GetDTDetails";
        $.ajax({
            type: "POST",
            url: "php/BillEntry.php",
            data: {'Req': Req, 'BillNo': BillNo},
            success: function (data) {
                $('#DTTableDIV').html(data);
            }
        })


}


function Reset() {
    $("#BReset").click(function () {
        // window.history.back();
        // location.reload();
        // window.location.reload(true);
        $("#FBillDt")[0].reset(0);
        var SlNo = 1;
        $('tr').remove();
        $('#TBillDt').append('<tr><th class="SrNoH">Sr No</th><th class="TestPropertyH">Test Group</th><th class="PropertyH">Test</th><th class="AmountH">Amount</th></tr>');
        $('#TBillDt').append('<tr class ="TRBillDt"><td><input type="text" id="SrNo_' + SlNo + '"  value= "' + SlNo + '" class="SrNo" name="SrNo_' + SlNo + '" value="1"></td><td><select type="text" id="TestGroup_' + SlNo + '" class="TestGroup" name="TestGroup_' + SlNo + '"></select></td><td><select  type="text" id="Test_' + SlNo + '" class="Test" name="Test_' + SlNo + '"></select></td><td><input type="text" id="Amount_' + SlNo + '" class="Amount" name="Amount_' + SlNo + '"></td></tr>');
        SetTestGroup();
        $("#SrNo_" + SlNo).attr("readonly", true);
        SetDate();
        SetMaxID();
        // $('tr').remove();
        // InsertRow();

    })
}
function Exit() {
    $("#BExit").click(function () {
        window.open('MainPageSCR.php', "_self");

    })
}
function SetSearchSCR() {
    ////set   serch window

    $("#SearchSCR").dialog({
        autoOpen: false,
        width: 700,
        height: 500,
        buttons: [
            {
                text: "Search",
                click: function () {

                    // alert($('#name').val());\
                    SearchItems();
                    //$( this ).dialog( "close" );
                }
            },
            {
                text: "Cancel",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });
}
function SearchItems() {
    var STxnDate = $("#STxnDate").val();
    var Sname = $("#SPname").val();
    //var SDname = $("#SDname").val();
    var SAdd1 = $("#SAdd1").val();
    var SPh = $("#SPhone").val();
    var Req = 'GetSearchItems';

    $.ajax({
        type: 'POST',
        url: 'php/BillEntry.php',
        data: {Req: Req, Sname: Sname, SAdd1: SAdd1, SPh: SPh, STxnDate: STxnDate},
        success: function (data) {
            // alert(data);a
            $("#SearchValues").html(data);
        }
    })





}
function Search() {
    ///open serach window
    $("#BSearch").click(function (event) {
        $("#SearchValues").html("");
        $("#SearchSCR").dialog("open");
        SetDate();
       
        event.preventDefault();
    });
}

function SerchItemClick() {
    $(document.body).on('dblclick', '.SearchResultRow', function () {
        var ID = $(this).attr('id');
        ID = ID.split('_');
        var SBillNo = $('#SBillNo_' + ID[1]).html();
        $('#BillNo').val(SBillNo);
        var Req = "GetHDDetails";
        var BillNo = $("#BillNo").val();
        
        // bill hd setting
        $.ajax({
            type: "POST",
            url: "php/BillEntry.php",
            data: {'Req': Req, 'BillNo': BillNo},
            //data: {'TestID': TestID, 'Req': Req},
            success: function (data) {
                var DataHD = data.split('~~');
                $('#TxnDate').val(DataHD[0]);
                $('#DocterID').val(DataHD[1]);
                $('#PatientName').val(DataHD[2]);
                $('#Age').val(DataHD[3]);
                $('#Sex').val(DataHD[4] * 1);
                $('#Address1').val(DataHD[5]);
                $('#Address2').val(DataHD[6]);
                $('#Address3').val(DataHD[7]);
                $('#PhoneNo').val(DataHD[8]);
                $('#TotalAmount').val(DataHD[9]);
                $('#AmountReceipt').val(DataHD[10]);
                $('#Discount').val(DataHD[11]);
                $('#Hospital').val(DataHD[12]);
                $('#Sample').val(DataHD[13]);
                 $("#SearchSCR").dialog("close");



            }
        })
        //bill dt detail setting
        var Req = "GetDTDetails";
        $.ajax({
            type: "POST",
            url: "php/BillEntry.php",
            data: {'Req': Req, 'BillNo': BillNo},
            success: function (data) {
                $('#DTTableDIV').html(data);
            }
        })



    })

}
function TableClick() {
   
    $(document.body).on('click', '#TBillDt', function () {
        TotalSet();

    });


}
function BillDateChange() {
    $('#TxnDate').change(function () {
        DateValidate('TxnDate');
    })
}
function SBillDateChange() {
    $('#STxnDate').change(function () {
        DateValidate('STxnDate');
    })
}

function SetDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = dd + '/' + mm + '/' + yyyy;
    $('#TxnDate').val(today);
    $('#STxnDate').val(today);
    
    //  document.getElementById("DATE").value = today;


}
function DocterIDChange() {
    //   $('#DocterID').change(function(){
    $(document.body).on('change', '#DocterID', function () {
        $('#Hospital').val("");

        GetHospital();





    })

}
function SetNewDocterSCR() {
    $("#NewDocterSCR").dialog({
        autoOpen: false,
        width: 300,
        height: 400,
        buttons: [
            {
                text: "Save",
                click: function () {

                    // alert($('#name').val());\
                    NewDocterMasterSave();
                    //$( this ).dialog( "close" );
                }
            },
            {
                text: "Exit",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });

}
function NewDocterMasterSave() {


    var DocterName = $('#DoctorName').val();
    var Hospital = $('#DHospital').val();
    var PhoneNo = $('#DPhoneNo').val();

    if (Validate("DoctorName") == 'N') {

        return;

    }
 var Req = 'Save1';
    $.ajax({
        type: 'POST',
        url: 'php/DoctorMaster.php',
        data: {Req: Req, DoctorName: DocterName, Hospital: Hospital, PhoneNo: PhoneNo},
        success: function (data) {
            // alert(data);
            SetDocterID();
            $("#DoctorMasterFID")[0].reset(0);

            var LastID = $('#DocterID option:last-child').val();

   
            $("#NewDocterSCR").dialog("close");

            LastID = LastID * 1;
            LastID = LastID + 1;
            alert(data);
            $("#DocterID").val(LastID);
            ////////////////////////////////////////////////////////////
            /////Set Hospita/////

            var DocterID = $('#DocterID').val();
            if (DocterID == "") {
                return;
            }
            var Req = 'GetHospital';
            $.ajax({
                type: "POST",
                url: "php/BillEntry.php",
                data: {'Req': Req, 'DocterID': DocterID},
                success: function (data) {
                    //  alert(data);
                    $('#Hospital').val(data);
                    $("#Hospital").attr("readonly", true);
                }

            }
            )

            ///////////////////////////////////////////////////////////

        }
    })
    /////Get Max ID ///



}
function  NewDocterClick() {
    $('#Add').click(function () {
        $("#NewDocterSCR").dialog("open");
        //DoctorMasterFID
        $("#DoctorMasterFID")[0].reset(0);

        // event.preventDefault();

    })

}
function NavigationBack(){
    $('#NavigationBack').click(
            function (){
                 var BillNo = $("#BillNo").val();
                  BillNo = (BillNo * 1) -1;
                NavigationSetdata(BillNo);         
            }
            )
}
function NavigationFront(){
    $('#NavigationFront').click(
            function (){
                 var BillNo = $("#BillNo").val();
                  BillNo = (BillNo *1) + 1;
                NavigationSetdata(BillNo);         
            }
            )
}

function GetHospital() {
    var DocterID = $('#DocterID').val();
    if (DocterID == "") {
        return;
    }
    var Req = 'GetHospital';
    $.ajax({
        type: "POST",
        url: "php/BillEntry.php",
        data: {'Req': Req, 'DocterID': DocterID},
        success: function (data) {
            //  alert(data);
            $('#Hospital').val(data);
            $("#Hospital").attr("readonly", true);
        }

    }
    )

}
///Set Print Dialog//
function SetPrintSCR() {
    $("#BillPrintSCR").dialog({
        autoOpen: false,
        width: 500,
        height: 400,
        buttons: [
            {
                text: "Print",
                click: function () {

                    // alert($('#name').val());\
                    BillPrint();
                    //$( this ).dialog( "close" );
                }
            },
            {
                text: "Exit",
                click: function () {
                    $(this).dialog("close");
                }
            }
        ]
    });

}
function PrintClick() {
    $('#BPrint').click(function () {
        BillPrint();
    })
}

function Print() {
    $("#BillPrintSCR").dialog("open");

    $("#BillPrintFID")[0].reset(0);
    $('#PrintFrame').contents().find("body").html(""); 
}
function  BillPrint() {

    var BillNo = $("#BillNo").val();
    var Date = $('#TxnDate').val();
    var Docter = $('#DocterID option:selected').text();
    var Patient = $('#PatientName').val();
    var Age = $('#Age').val();
    var Sex = $('#Sex option:selected').text();
    var Address = $('#Address1').val() + ',' + $('#Address2').val() + ',' + $('#Address3').val();
    var TotalAmount = $('#TotalAmount').val();
    var Sample = $('#Sample').val();
    var PrintData = '';
    var LabDetails = GetLabDetails();
    LabDetails = LabDetails.split("~~");
    
    PrintData = PrintData + '<p align=center style="font-size:20px;top:20px;line-height:50%;"><b>' + LabDetails[0] + '</b></p>';
    PrintData = PrintData + '<p align=center style="font-size:20px;top:20px;line-height:50%;">' + LabDetails[1] + '</p>';
    PrintData = PrintData + '<p align=center style="font-size:20px;top:20px; line-height:50%;">Phone:' + LabDetails[2] + '</p>';


    PrintData = PrintData + '<p style="position: absolute;left :100px; top:80px;"><b>Ref.ID&nbsp;&nbsp;:&nbsp;</b>' + BillNo + '</p>';
    PrintData = PrintData + '<p style="position: absolute;left :100px; top:110px;" ><b>Patient&nbsp;:&nbsp;</b>' + Patient + '</p>';
    PrintData = PrintData + '<p style="position: absolute;left :100px; top:140px;"><b>Age&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</b>' + Age + '</p>';


    PrintData = PrintData + '<br><p style="position: absolute;left :570px; top:80px;"><b>Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</b>' + Date + '</p>';
    PrintData = PrintData + '<br><p style="position: absolute;left :570px; top:110px;"><b>Doctor&nbsp;:&nbsp;</b>' + Docter + '</p>';
    PrintData = PrintData + '<br><p style="position: absolute;left :570px; top:140px;"><b>Sex&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;</b>' + Sex + '</p>';

    PrintData = PrintData + '<hr width="800px" style="position: absolute; top:170px;"></hr>';

    PrintData = PrintData + '<p style=" position: absolute;left:100px; top:172px; font-size:20px; line-height:40%;"><b>Sr No&nbsp;</b></p>';
    PrintData = PrintData + '<p style=" position: absolute;left:200px; top:172px; font-size:20px; line-height:40%;"><b>Test&nbsp;</b></p>';
    PrintData = PrintData + '<p style=" position: absolute;left:600px; top:172px; font-size:20px; line-height:40%;"><b>Amount&nbsp;</b></p>';

    PrintData = PrintData + '<hr width="800px" style="position: absolute; top:200px;"></hr>';


    var i = 0;
    var TableLength = ($(".TRBillDt")).length;

    if ($('#TestGroup_1').length > 0) {////Check and apply Un Saved Bills Table Length less one from user inter face
        TableLength = TableLength - 1;
    }
    PrintData=PrintData+'<br><br>';
    for (i = 1; i <= TableLength; i++) {

        if ($('#TestGroup_1').length > 0) { //check Not Saved Bill 
            //Get Not Saved Table detail
            PrintData = PrintData + '<br><p style="position: absolute;left :110px; line-height:40%;">' + $('#SrNo_' + i).val() + '</p>';
            PrintData = PrintData + '<p style="position: absolute;left:200px; line-height:40%;">' + $('#Test_' + i + ' option:selected').text() + '</p>';
            PrintData = PrintData + '<p style="position: absolute;left:610px; line-height:40%;">' + $('#Amount_' + i).val() + '</p>';
            PrintData = PrintData + '<br>';


        }
        else {
            PrintData = PrintData + '<br><p style="position:absolute;left:110px; line-height:40%; ">' + $('#SrNo_' + i).html() + '</p>';
            PrintData = PrintData + '<p style="position: absolute;left :200px; line-height:40%;">' + $('#Test_' + i).html() + '</p>';
            PrintData = PrintData + '<p style="position: absolute;left :610px; line-height:40%;">' + $('#Amount_' + i).html() + '</p>';
            PrintData = PrintData + '<br>';

        }

    }
    PrintData = PrintData + '<br><hr width="800px" style="position: absolute;"></hr>';
    PrintData = PrintData + '<br style="line-height:10px;"><p style="position:absolute;left:560px; line-height:40%;"><b>Total&nbsp;:&nbsp;&nbsp;<u>' + TotalAmount + '</u></b></p>';
    PrintData = PrintData + '<br ><br><hr width="800px" style="position: absolute;"></hr>';
    PrintData = PrintData + '<br><p style="position:absolute;left:450px;">For&nbsp;' + LabDetails[0] + '</p>';
    // PrintData=PrintData+'<br><p style="position: absolute;left :30px; top:160px;"><b>Age:&nbsp;</b>'+Age+'</p>';
    //PrintData=PrintData+'<br><p><b>Sex:&nbsp;</b>'+Sex+'</p>';


    //PrintData=PrintData+'<br><p><b>Sample:&nbsp;</b>'+Sample+'</p>';


    $('#PrintFrame').contents().find("body").html(PrintData);
    window.frames["PrintFrame"].focus();
    window.frames["PrintFrame"].print();
    // var $body=$('body',doc);
    // $body.html('<h1>hai123</h1>');






}
function GetLabDetails() {
    var Req = 'GetLabDetails';

    var LabDetails = "";
    $.ajax({
        type: 'POST',
        url: 'php/LabMaster.php',
        async: false,
        data: {Req: Req},
        success: function (data) {

            LabDetails = data;


        }

    })
    return LabDetails;

}
function Cancel(){
    $('#BCancel').click(function(){
        var BillNo = $('#BillNo').val();
         var temp =confirm("Do You Wand To cancel  Bill No :"+ BillNo);
       //  alert(temp);
         if(temp == true){
        var Req='CancelBill';
        $.ajax({
            type:"POST",
            url:"php/BillEntry.php",
            data:{'Req':Req,'BillNo':BillNo },
            success:function(data){ 
                alert(data);
                // Reset();
                 $("#FBillDt")[0].reset(0);
                var SlNo = 1;
                $('tr').remove();
                $('#TBillDt').append('<tr><th class="SrNoH">Sr No</th><th class="TestPropertyH">Test Group</th><th class="PropertyH">Test</th><th class="AmountH">Amount</th></tr>');
                $('#TBillDt').append('<tr class ="TRBillDt"><td><input type="text" id="SrNo_' + SlNo + '"  value= "' + SlNo + '" class="SrNo" name="SrNo_' + SlNo + '" value="1"></td><td><select type="text" id="TestGroup_' + SlNo + '" class="TestGroup" name="TestGroup_' + SlNo + '"></select></td><td><select  type="text" id="Test_' + SlNo + '" class="Test" name="Test_' + SlNo + '"></select></td><td><input type="text" id="Amount_' + SlNo + '" class="Amount" name="Amount_' + SlNo + '"></td></tr>');
                SetTestGroup();
                $("#SrNo_" + SlNo).attr("readonly", true);
                SetDate();
                SetMaxID();
            }     
        })
        
    }
    }  )
    
    
}
function SetReadOnly(){
    
     $("#TotalAmount").attr("readonly", true);
     $("#AmountReceipt").attr("readonly", true);
}

$(document).ready(function () {
    SetDocterID();
    SetSex();
    //InsertRow();
    BillDtChange();
    SetTestGroup();
    // SetTestID();
    SetMaxID();
    TestGroupChange();
    TestIDChange();
    BillNoChange();
    Save();
    Reset();
    Exit();
    SetSearchSCR();
    Search();
    SerchItemClick();
    TableClick();
    DiscountChange();
    ReceiptAmount();
    AmountChange();
    BillDateChange();
    SetDate();
    DocterIDChange();
    SetNewDocterSCR();
    //  NewDocterMasterSave();
    NewDocterClick();
    $("#SrNo_1").attr("readonly", true)
    var title = $(this).attr('title');
    //$('#PageHeader').html('<h1 class=TitleName>' + title + '</h1>');
     $('#ScrName').html('<h1 class=TitleName>'+title+'</h1>');
    SetPrintSCR();
    PrintClick();
     $('#DocterID').focus();
     Cancel();
     SBillDateChange();
     SetReadOnly();
     NavigationBack();
     NavigationFront()
    

    ///$('#PrintFrame').hide();


});