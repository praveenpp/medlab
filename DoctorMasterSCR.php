<?php
include 'MainPageSCR.php';
?>
<html>
    <head>
        <title>Doctor Master</title>
        <script type = "text/javascript" src="js/jquery.js"></script>
          <script type = "text/javascript" src="js/jquery-ui.min.js"></script>
        <script type = "text/javascript" src="js/DoctorMaster.js"></script>
        <script type = "text/javascript" src="js/FormGeneral.js"></script>
          <link href="css/jquery-ui.min.css" rel="stylesheet">
    </head>
    <body>
        <div id ="MainScrBody">
            <form id="DoctorMasterFID">
                <label for = "DoctorID"> Doctor ID</label>
                <input type ="text" id = "DoctorID" name="DoctorID" class="textbox" >

                <br>
                <br>

                <label for ="DoctorName">Doctor Name</label>
                <input type = "text" id = "DoctorName" name="DoctorName" class="textboxbig">

                <br>
                <br>

                <label for ="Hospital">Hospital</label>
                <input type = "text" id = "Hospital" name="Hospital" class="textboxbig">

                <br>
                <br>


                <label for ="PhoneNo">Phone No</label>
                <input type = "text" id = "PhoneNo" name="PhoneNo" class="textboxbig">

                <br>
            </form>
              <div id="SearchSCR" title="Search Doctor">
                    <label >Doctor</label> <br>
                    <input type="text"  id="SDoctor" value="" >
                    <br><br>
                    <div id="SearchValues"></div>

                </div>
        </div>
        <div id ="ScreenButtons">
            <input type = "button"  class="button"  id = "BSave" value = "Save">
            <br><br>
             <input type = "button" class="button" id = "BSearch" value = "Search">
            <br><br>
            <input type = "button" class="button" id="BReset" name ="Reset" value="Reset">
            <br><br>
         <input type = "button" class="button" id="BExit" name ="Exit" value="Exit">
        </div>
    </body>
</html>